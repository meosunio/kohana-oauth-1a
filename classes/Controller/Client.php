<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Client extends Controller
{
	const API_KEY = '87417e7aa45f8d70efa9880fc836b6cd7a20c47f';
	const SECRET_KEY = 'a6ddda201534cbc4263607b3e115b68e29a7f9e2';
	
	public function before()
	{
		$this->auto_render = false;		
	}
	
	
	public function action_index()
	{
		$consumer = new Oauth_Consumer(self::API_KEY, self::SECRET_KEY);
	
		$req_req = Oauth_Request::from_consumer_and_token(
			$consumer,
			NULL,
			"GET",
			"http://beta999.mmvpay.com/server/request/token",
			array(
				'x-auth-username' => 'romeo+200@matchmove.com',
				'x-auth-password' => 'password123'
			)
		);
		
		$req_req->sign_request(new Oauth_Signature_HMACSHA1(), $consumer, NULL);
	
		echo $req_req;
	}
	
	public function action_access_token()
	{
		$consumer = new Oauth_Consumer(self::API_KEY, self::SECRET_KEY);
		
		$req_tokem = new Oauth_Token('44103e6d1d9504ceb3a02b66fce85969', '291b9f0f1e18d9733f836ce65ade87be');
		$acc = Oauth_Request::from_consumer_and_token($consumer, $req_tokem, "GET", "http://beta999.mmvpay.com/server/access/token");
		$acc->sign_request(new Oauth_Signature_HMACSHA1(), $consumer, $req_tokem);
		
		echo $acc;
	}
	
	public function action_call_api()
	{
		$consumer = new Oauth_Consumer(self::API_KEY, self::SECRET_KEY);
		
		$acc_token = new Oauth_Token('f05e32b9c1ff8533467a4dd9f2ba4612', 'ff8e92f77bc1e215981f08da28759101');		
		$echo_req = Oauth_Request::from_consumer_and_token($consumer, $acc_token, "GET", 'http://beta999.mmvpay.com/server', array("method"=> "foo%20bar", "bar" => "baz"));
		$echo_req->sign_request(new Oauth_Signature_HMACSHA1(), $consumer, $acc_token);
		
		echo $echo_req;
	}
	
}
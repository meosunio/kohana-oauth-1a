<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Server extends Controller
{
	
	protected $_server = null;
	public function before()
	{
		$this->auto_render  = false;
		
		$data_store = new Oauth_Datastore();
		$this->_server = new Oauth_Server($data_store);
		
		$hmac_method = new Oauth_Signature_HMACSHA1();
		$plaintext 	 = new Oauth_Signature_PLAINTEXT();
		
		$this->_server->add_signature_method($hmac_method);
		$this->_server->add_signature_method($plaintext);
	}
	
	public function action_index()
	{
		try
		{
			$req = OAuth_Request::from_request();
			list($consumer, $token) = $this->_server->verify_request($req);
		  
			// lsit back the non-OAuth params
			$total = array();
			foreach($req->get_parameters() as $k => $v)
			{
				if (substr($k, 0, 5) == "oauth" || $k == 'x-device-hash') continue;
				
				$total[] = urlencode($k) . "=" . urlencode($v);
			}
			print implode("&", $total);
		  }
		  catch (OAuth_Exception_Response $e)
		  {
			print($e->getMessage() . "\n<hr />\n");
			print_r($req);
			die();
		  }
		  
	}
			  
	public function action_request_token()
	{
		$req = OAuth_Request::from_request();
		
		try
		{
			$req = Oauth_Request::from_request();
			$token = $this->_server->fetch_request_token($req);
			
			print $token;
		}
		catch (OAuth_Exception_Response $e)
		{
			print($e->getMessage() . "\n<hr />\n");
			print_r($req);
			die();
	  }
		
	}
	
	public function action_access_token()
	{
		try
		{
			$req = Oauth_Request::from_request();
			$token = $this->_server->fetch_access_token($req);
			print $token;
		}
		catch (OAuth_Exception_Response $e)
		{
			print($e->getMessage() . "\n<hr />\n");
			print_r($req);
			die();
		}

		
	}
}
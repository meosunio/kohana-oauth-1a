<?php defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Oauth_Abstract_Consumer
{
	public $key;
	public $secret;
	/**
	 * Class Constructor
	 *
	 * @param	string	$key			Consumer Key
	 * @param	string	$secret			Cosumer Secret
	 * @param	string	$callback_url	Callback url
	 */
	public function __construct($key, $secret)
	{
		$this->key = $key;
		$this->secret = $secret;
	}

	/**
	 * Magic Method
	 */
	public function __toString()
	{
		return "Oauth_Consumer[key=$this->key,secret=$this->secret]";
	} 
}
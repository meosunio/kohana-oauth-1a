<?php defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Oauth_Abstract_Datastore
{
	/**
     * Search for the Consumer record using consumer key(api key)
     *
     * @param	string	$consumer_key	Consumer API Key
     * 
     * @return ORM|null
     */
    abstract public function lookup_consumer($consumer_key);
	
	/**
     * Search for the Request/Access record
     *
     * @param	object	$consumer		Consumer Class Instance
     * @param	string 	$token_type		Token Type (Access/Request)
     * @param	string	$token			Token Value
     * @return ORM|null
     */
    abstract public function lookup_token($consumer, $token_type, $token);

	/**
     * Search for the Nonce record
     *
     * @param	object	$consumer		Consumer Class Instance
     * @param	string 	$token			OAuth Token Instance
     * @param	string	$nonce			Nonce Value
     * @param	int		$timestamp		Timestamp
     * 
     * @return boolean
     */
    abstract public function lookup_nonce($consumer, $token, $nonce, $timestamp);
	
	/**
     * Creates a new Request Token
     *
     * @param	object	$consumer		Consumer Class Instance
     * @param	string 	$callback		Callback URL
     * 
     * @return ORM|null
     */
    abstract public function new_request_token($consumer, $callback = null);
	
	/**
     * Search for the Request/Access record record
     *
     * @param	string 	$token			OAuth Token Instance
     * @param	object	$consumer		Consumer Class Instance
     * @param	string	$verifier		Request Token Verfier
     * 
     * @return ORM|null
     */
    abstract public function new_access_token($token, $consumer, $verifier = null);

}
<?php defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Oauth_Abstract_Encrypt
{
	/**
	 * Cipher
	 */
	const _CIPHER = MCRYPT_RIJNDAEL_128;
	
	/**
	 * Mode
	 */
	const _MODE =  MCRYPT_MODE_NOFB;
	
	/**
	 * Rand
	 */
	const _RAND = MCRYPT_RAND;
	
	/**
	 * Public Class Constructor
	 */
	public function __construct(){}
	
	/**
	 * Encrypt data
	 *
	 * @param	string	$data		Data that will be encrypted
	 * @param	string	$key		Encryption Secret key
	 *
	 * @return	string
	 */
	public static function encode($data, $key)
	{
		//generate iv size
		$iv_size = mcrypt_get_iv_size(Oauth_Encrypt::_CIPHER, Oauth_Encrypt::_MODE);
		
		//generate iv
		$iv = mcrypt_create_iv($iv_size, Oauth_Encrypt::_RAND);
		
		//encode data using AES-128(rijndael-128)
		$data = mcrypt_encrypt(Oauth_Encrypt::_CIPHER, $key, $data, Oauth_Encrypt::_MODE, $iv);
		
		//return encoded data
		return base64_encode($iv. $data);
	}
	
	/**
	 * Decrypt data
	 *
	 * @param	string	$data		Data that will be encrypted
	 * @param	string	$key		Encryption Secret key
	 * @param 	string	$cipher		encryption cipher, one of the Mcrpyt cipher constants
	 * @param	string	$mode		encryption mode, one of MCRYPT_MODE_*
	 *
	 * @return	string
	 */
	public static function decode($data, $key)
	{
		// Convert the data back to binary
		$data = base64_decode($data, true);
	 
		if (!$data)
		{
			// Invalid base64 data
			return FALSE;
		}
		
		//generate iv size
		$iv_size = mcrypt_get_iv_size(Oauth_Encrypt::_CIPHER, Oauth_Encrypt::_MODE);
		
		// Extract the initialization vector from the data
		$iv = substr($data, 0, $iv_size);
	 
		if ($iv_size !== strlen($iv))
		{
			// The iv is not the expected size
			return FALSE;
		}
	 
		// Remove the iv from the data
		$data = substr($data, $iv_size);
	 
		// Return the decrypted data, trimming the \0 padding bytes from the end of the data
		return rtrim(mcrypt_decrypt(Oauth_Encrypt::_CIPHER, $key, $data, Oauth_Encrypt::_MODE, $iv), "\0");
	}
}
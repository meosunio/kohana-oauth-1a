<?php defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Oauth_Abstract_Server
{
	protected $timestamp_threshold = 300; // in seconds, five minutes
	protected $version = '1.0'; 
	protected $signature_methods = array();
	
	protected $data_store;
	
	public function __construct($data_store)
	{
		$this->data_store = $data_store;
	}
	
	public function add_signature_method($signature_method)
	{
		$this->signature_methods[$signature_method->get_name()] = $signature_method;
	}
	
	// high level functions
	/**
	 * process a request_token request
	 * returns the request token on success
	 */
	public function fetch_request_token(&$request)
	{
		$this->get_version($request);
	  
		$consumer = $this->get_consumer($request);
		
		// no token required for the initial token request
		$token = NULL;
		
		//user specific parameters
		$device_hash = $request->get_parameter('x-device-hash');
		if (empty($device_hash))
		{
			throw new Oauth_Exception_Response('Missing parameters `x-device-hash`.');
		}
	  
		//check request signature
		$this->check_signature($request, $consumer, $token);
	  
		// Rev A change
		$callback = $request->get_parameter('oauth_callback');
		
		//set request signature and client's secret key as decryption key
		$decryption_key = md5($request->get_parameter('oauth_signature') . $consumer->secret);
		
		//verify user credentials
		$user = ORM::factory('User')->verified_user(
			$request->get_parameter('x-auth-username'),
			$request->get_parameter('x-auth-password'),
			$decryption_key
		);
		
		//checks if a valid user
		if (empty($user))
		{
			throw new Oauth_Exception_Response('Invalid username or password');
		}
		
		//create new request token
		$new_token = $this->data_store->new_request_token($consumer, $callback, $user, $device_hash);
	  
		return $new_token;
	}
	
	/**
	 * process an access_token request
	 * returns the access token on success
	 */
	public function fetch_access_token(&$request)
	{
		$this->get_version($request);
	  
		$consumer = $this->get_consumer($request);
	  
		// requires authorized request token
		$token = $this->get_token($request, $consumer, "request");
	  
		//check signatire
		$this->check_signature($request, $consumer, $token);
	  
		// Rev A change
		$verifier = $request->get_parameter('oauth_verifier');
		
		//create a new access token
		$new_token = $this->data_store->new_access_token($token, $consumer, $verifier);
	  
		return $new_token;
	}
	
	/**
	 * verify an api call, checks all the parameters
	 */
	public function verify_request(&$request)
	{
		//get api version
		$this->get_version($request);
		
		//get consumer record	
		$consumer = $this->get_consumer($request);
		
		//verify access token
		$token = $this->get_token($request, $consumer, "access");
		
		//check signatire
		$this->check_signature($request, $consumer, $token);
		
		//if everything is ok, return the consumer and token info
		return array($consumer, $token);
	}
	
	// Internals from here
	/**
	 * version 1
	 */
	protected function get_version(&$request)
	{
		$version = $request->get_parameter("oauth_version");
		if (!$version)
		{
			// Service Providers MUST assume the protocol version to be 1.0 if this parameter is not present. 
			// Chapter 7.0 ("Accessing Protected Ressources")
			$version = '1.0';
		}
		
		if ($version !== $this->version)
		{
			throw new Oauth_Exception_Response("OAuth version '$version' not supported");
		}
		
		return $version;
	}
	
	/**
	 * figure out the signature with some defaults
	 */
	protected function get_signature_method($request) {
		$signature_method = $request instanceof Oauth_Request 
			? $request->get_parameter("oauth_signature_method")
			: NULL;
	  
		if (!$signature_method)
		{
			// According to chapter 7 ("Accessing Protected Ressources") the signature-method
			// parameter is required, and we can't just fallback to PLAINTEXT
			throw new Oauth_Exception_Response('No signature method parameter. This parameter is required');
		}
	  
		if (!in_array($signature_method, array_keys($this->signature_methods)))
		{
			throw new Oauth_Exception_Response(
				"Signature method '$signature_method' not supported " .
				"try one of the following: " .
				implode(", ", array_keys($this->signature_methods))
			);
		}
		
		return $this->signature_methods[$signature_method];
	}
	
	/**
	 * try to find the consumer for the provided request's consumer key
	 */
	protected function get_consumer($request)
	{
		$consumer_key = $request instanceof Oauth_Request 
			? $request->get_parameter("oauth_consumer_key")
			: NULL;
	  
		if (!$consumer_key)
		{
			throw new Oauth_Exception_Response("Invalid consumer key");
		}
	  
		$consumer = $this->data_store->lookup_consumer($consumer_key);
		if (!$consumer)
		{
			throw new Oauth_Exception_Response("Invalid consumer");
		}
	  
		return $consumer;
	}
	
	/**
	 * try to find the token for the provided request's token key
	 */
	protected function get_token($request, $consumer, $token_type="access")
	{
		$token_field = $request instanceof Oauth_Request
			 ? $request->get_parameter('oauth_token')
			 : NULL;
	  
		$token = $this->data_store->lookup_token(
			$consumer, $token_type, $token_field, $request->get_parameter('x-device-hash')
		);
		
		if (!$token)
		{
			throw new Oauth_Exception_Response("Invalid $token_type token: $token_field");
		}
		
		return $token;
	}
	
	/**
	 * all-in-one function to check the signature on a request
	 * should guess the signature method appropriately
	 */
	protected function check_signature($request, $consumer, $token)
	{
		// this should probably be in a different method
		$timestamp = $request instanceof Oauth_Request
			? $request->get_parameter('oauth_timestamp')
			: NULL;
			
		$nonce = $request instanceof Oauth_Request
			? $request->get_parameter('oauth_nonce')
			: NULL;
	  
		$this->check_timestamp($timestamp);
		$this->check_nonce($consumer, $token, $nonce, $timestamp);
	  
		$signature_method = $this->get_signature_method($request);
	  
		$signature = $request->get_parameter('oauth_signature');
		$valid_sig = $signature_method->check_signature(
			$request,
			$consumer,
			$token,
			$signature
		);
	  
		if (!$valid_sig)
		{
			throw new Oauth_Exception_Response("Invalid signature");
		}
	}
	
	/**
	 * check that the timestamp is new enough
	 */
	protected function check_timestamp($timestamp)
	{
		if( ! $timestamp )
		{
			throw new Oauth_Exception_Response(
				'Missing timestamp parameter. The parameter is required'
			);
		}
		
		// verify that timestamp is recentish
		$now = time();
		if (abs($now - $timestamp) > $this->timestamp_threshold)
		{
			throw new Oauth_Exception_Response(
				"Expired timestamp, yours $timestamp, ours $now"
			);
		}
	}
	
	/**
	 * check that the nonce is not repeated
	 */
	protected function check_nonce($consumer, $token, $nonce, $timestamp)
	{
		if( ! $nonce )
		{
			throw new Oauth_Exception_Response(
				'Missing nonce parameter. The parameter is required'
			);
		}
		
		// verify that the nonce is uniqueish
		$found = $this->data_store->lookup_nonce(
			$consumer,
			$token,
			$nonce,
			$timestamp
		);
		
		if ($found)
		{
			throw new Oauth_Exception_Response("Nonce already used: $nonce");
		}
	}

}
	
	

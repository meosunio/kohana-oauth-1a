<?php defined('SYSPATH') or die('No direct script access.');

class Oauth_Datastore extends Kohana_Oauth_Abstract_Datastore
{
	/**
	 * acceptable token types
	 */
	protected static $_valid_token_type = array('access', 'request');
	
	/**
     * Search for the Consumer record using consumer key(api key)
     *
     * @param	string	$consumer_key	Consumer API Key
     * 
     * @return object|null
     */
    public function lookup_consumer($consumer_key)
	{
		//search consumer via api key
		$consumer = self::_consumer()->search_by('key', $consumer_key);
		if (!$consumer->loaded())
		{
			return null;
		}
		
		return new Oauth_Consumer($consumer->key, $consumer->secret);
	}
	
	/**
     * Search for the Request/Access record
     *
     * @param	object	$consumer		Consumer Class Instance
     * @param	string 	$token_type		Token Type (Access/Request)
     * @param	string	$token			Token Value
     * @return object|null
     */
    public function lookup_token($consumer, $token_type, $token, $user_device_hash = null)
	{
		$token_type = strtolower(trim($token_type));
		if (!in_array($token_type, self::$_valid_token_type))
		{
			throw new Oauth_Exception_Response("Invalid Token Lookup parameter: ". $token_type);
		}
		
		$token_object = ($token_type === 'request')
						? self::_request_token()
						: self::_access_token();
		
		$token_record = $token_object
						->where('key', '=', $token)
						->where('consumer_key', '=', $consumer->key)
						->where('device_hash', '=', $user_device_hash)
						->find();

		return (!$token_record->loaded())
				? null
				: new Oauth_Token($token_record->key, $token_record->secret);
	}

	/**
     * Search for the Nonce record
     *
     * @param	object	$consumer		Consumer Class Instance
     * @param	string 	$token			OAuth Token Instance
     * @param	string	$nonce			Nonce Value
     * @param	int		$timestamp		Timestamp
     * 
     * @return boolean
     */
    public function lookup_nonce($consumer, $token, $nonce, $timestamp)
	{
		//look for nonce associated with the consumer key
		$nonce_record = self::_nonce()
						->where('nonce', '=', $nonce)
						->where('consumer_key', '=', $consumer->key)
						->find();
		if ($nonce_record->loaded())
		{
			return true;
		}
		
		//information that will be save
		$parameters = array(
			'nonce' => $nonce,
			'consumer_key' => $consumer->key
		);
		
		//create a nonce record
		$nonce_record->data_save($parameters);
		
		return false;
	}
	
	/**
     * Creates a new Request Token
     *
     * @param	object	$consumer		Consumer Class Instance
     * @param	string 	$callback		Callback URL
     * @param	object	$user			ORM User instance
     * 
     * @return object|null
     */
    public function new_request_token($consumer, $callback = null, $user = null, $user_device_hash = null)
	{
		//fallback
		if (empty($user) || !$user->loaded())
		{
			throw new Oauth_Exception_Response("Invalid username or password");
		}
		
		$parameters = array(
			'key' 			=> self::generate_token($consumer->key),
			'secret' 		=> self::generate_token($consumer->key),
			'consumer_key' 	=> $consumer->key,
			'token_verifier'=> self::generate_token($consumer->key),
			'callback_url'	=> $callback,
			'user_id'		=> $user->id,
			'device_hash'	=> $user_device_hash,
			'scope'	=> 'all'
		);
	
		//create a request token record
		$request_token = self::_request_token()->data_save($parameters);
		if (!$request_token->saved())
		{
			throw new Oauth_Exception_Response("Unable to generate a request token");
		}
		
		return new Oauth_Token($request_token->key, $request_token->secret);
	}
	
	/**
     * Search for the Request/Access record record
     *
     * @param	string 	$token			OAuth Token Instance
     * @param	object	$consumer		Consumer Class Instance
     * @param	string	$verifier		Request Token Verfier
     * 
     * @return ORM|null
     */
    public function new_access_token($token, $consumer, $verifier = null)
	{
		$request_token = self::_request_token()
					->where('key', '=', $token->key)
					->where('consumer_key', '=', $consumer->key);
		if (!empty($verifier))
		{
			$request_token->where('token_verifier', '=', $verifier);
		}
		
		$request_token = $request_token->find();
		if (!$request_token->loaded())
		{
			throw new Oauth_Exception_Response("Invalid request token");
		}
		
		$parameters = array(
			'key' 			=> self::generate_token($consumer->key),
			'secret' 		=> self::generate_token($consumer->key),
			'consumer_key' 	=> $consumer->key,
			'user_id'		=> $request_token->user_id,
			'device_hash'	=> $request_token->device_hash,
			'scope'			=> $request_token->scope
		);
	
		//create a request token record
		$access_tokem = self::_access_token()->data_save($parameters);
		if (!$access_tokem->saved())
		{
			throw new Oauth_Exception_Response("Unable to generate an access token");
		}
		
		//delete request token record
		$request_token->delete();
		
		//return Access token
		return new Oauth_Token($access_tokem->key, $access_tokem->secret);
	}
	
	/**
	 * Generates a unique md5 encoded string
	 *
	 * @param	string	$prefix	Prefix string
	 *
	 * @return	string
	 */
	public static function generate_token($prefix = null)
	{
		return md5($prefix. uniqid(time(), true). time());
	}
	
	/**
	 * Returns an instance of Consumer ORM Object
	 *
	 * @return ORM Object
	 */
	protected static function _consumer()
	{
		return ORM::factory('Consumer');
	}
	
	/**
	 * Returns an instance of Access Token ORM Object
	 *
	 * @return ORM Object
	 */
	protected static function _access_token()
	{
		return ORM::factory('Access_Token');
	}
	
	/**
	 * Returns an instance of Request Token ORM Object
	 *
	 * @return ORM Object
	 */
	protected static function _request_token()
	{
		return ORM::factory('Request_Token');
	}
	
	/**
	 * Returns an instance of Nonce ORM Object
	 *
	 * @return ORM Object
	 */
	protected static function _nonce()
	{
		return ORM::factory('Nonce');
	}  
}
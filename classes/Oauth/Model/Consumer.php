<?php defined('SYSPATH') OR die('No direct access allowed.');

class Oauth_Model_Consumer extends Oauth_ORM {
	
	//set table's primary key
	protected $_primary_key = 'id';
	
	//specify table name
	protected $_table_name = 'oauth_consumer';
	
	//table fields/column
	protected $_table_columns = array(
		'id' 			=> NULL,
		'key' 			=> NULL,
		'secret'  		=> NULL,
		'name'			=> NULL,
		'date_added' 	=> NULL
    );
	
	//list of validation rules
	public function rules()
    {
        return array(
			'key' 			=> array(
				array('not_empty')
			),
			'secret'  		=> array(
				array('not_empty')
			),
			'name'  		=> array(
				array('not_empty')
			)
        );
    }
}
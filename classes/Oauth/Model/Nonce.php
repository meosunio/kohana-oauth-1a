<?php defined('SYSPATH') OR die('No direct access allowed.');

class Oauth_Model_Nonce extends Oauth_ORM {
	
	//set table's primary key
	protected $_primary_key = 'nonce';
	
	//specify table name
	protected $_table_name = 'oauth_nonce';
	
	//table fields/column
	protected $_table_columns = array(
		'nonce' 		=> NULL,
		'consumer_key'	=> NULL,
		'date_added' 	=> NULL
    );
	
	//list of validation rules
	public function rules()
    {
        return array(
			'nonce' => array(
				array('not_empty')
			),
			'consumer_key' => array(
				array('not_empty')
			)
        );
    }
}
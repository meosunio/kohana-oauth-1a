<?php defined('SYSPATH') OR die('No direct access allowed.');

class Oauth_Model_Request_Token extends Oauth_ORM {
	
	//set table's primary key
	protected $_primary_key = 'id';
	
	//specify table name
	protected $_table_name = 'oauth_request_token';
	
	//table fields/column
	protected $_table_columns = array(
		'id' 				=> NULL,
		'key' 				=> NULL,
		'secret'  			=> NULL,
		'consumer_key' 		=> NULL,
		'token_verifier' 	=> NULL,
		'user_id' 			=> NULL,
		'device_hash'  		=> NULL,
		'callback_url' 		=> NULL,
		'scope'				=> NULL,
		'date_added' 		=> NULL
    );
	
	//list of validation rules
	public function rules()
    {
        return array(
			'key' => array(
				array('not_empty')
			),
			'secret' => array(
				array('not_empty')
			),
			'token_verifier' => array(
				array('not_empty')
			),
			'consumer_key' => array(
				array('not_empty')
			),
			'device_hash' => array(
				array('not_empty')
			),
			'user_id' => array(
				array('not_empty')
			)
        );
    }
}
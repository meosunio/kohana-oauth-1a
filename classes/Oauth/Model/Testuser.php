<?php defined('SYSPATH') OR die('No direct access allowed.');

class Oauth_Model_Testuser extends Oauth_ORM {
	
	
	//set table's primary key
	protected $_primary_key = 'user_id';
	
	//specify table name
	protected $_table_name = 'testuser';
	
	//table fields/column
	protected $_table_columns = array(
		'user_id'  => NULL,
		'username' => NULL,
		'password' => NULL
    );
	
	//list of validation rules
	public function rules()
    {
        return array(
        );
    }
	
	/**
	 * Decodes and verffies user's password
	 *
	 * @param	string	$username		Encrypted Username
	 * @param	string	$password		Encrypted User password
	 * @param	string	$decode_key		Decryption key
	 *
	 * @return	boolean
	 */
	protected function verify_user($username, $password, $decode_key)
	{
		//decode username
		$username = Oauth_Encrypt::decode($username, $decode_key);
		
		//checks if user record exists, else throws an exception
		$user = $this->search_by('username', $username);
		
		$xx = Auth::instance();
		//decode password(plain text password)
		$password = Oauth_Encrypt::decode($password, $decode_key);
		
		var_dump($password, $user->password, $xx->hash($password));
		
		return ($user->password === Auth::instance()->hash($password))
				? $user
				: false;
	}
	
	/**
	 * Decodes and verffies user's password
	 *
	 * @param	string	$password		Encrypted User password
	 * @param	string	$decode_key		Decryption key
	 *
	 * @return	boolean
	 */
	public function verified_user($username, $password, $decode_key)
	{
		return $this->verify_user($username, $password, $decode_key);
	}
}
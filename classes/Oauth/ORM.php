<?php defined('SYSPATH') OR die('No direct access allowed.');

class Oauth_ORM extends ORM {
    
    //error message
    public static $message = null;
    
    /**
     * This function throws the first validation error
     *
     * @param   object  $e  Exception object
     * @return Exception
     */
    protected function _throw_error($e)
    {
        $exception_class = get_class($e);
        
        switch ($exception_class)
        {
            case 'ORM_Validation_Exception':
                //location of models error message
                $file = strtolower(str_replace('_', '/', get_class($this)));
                
                // Loads error messages 
                $errors = $e->errors($file);
                
                //returns first error message
                self::$message = array_shift($errors);
                
                break;
            
            default:
                self::$message = $e->getMessage();
        }
        
        //throw an http exception
        throw HTTP_Exception::factory(400, self::$message);    
    }
    
    /**
	 * Loads the data for a request token based on the token string.
	 * 
	 * @throws 	DataStoreReadException
	 * 
	 * @param 	string		$token	Access Token
	 * @return 	ORM Object
	 */
	public function search_by($field, $value)
	{
        try
        {
            $result = $this->where($field, '=', $value)->find();
            
            if (!$result->loaded())
            {
                throw new Exception('Record not found.');
            }
        
            return $result;
        }
        catch(Exception $e)
        {
            //throw excetion error
			$this->_throw_error($e);
        }
	}
    
    /**
	 * Create/Update Access Token entry
	 *
	 * @throws 	HTTP_Excetion
	 * @param	array			$parameters		Array of paramters
	 * @return object
	 */
	public function data_save($parameters)
	{
		try
		{
			//remove all whitespace 
			$parameters = array_map('trim', $parameters);
			
			//get all valid fields
			$vaid_fields = array_keys(array_intersect_key(
				$parameters,
				$this->_table_columns
			));
			
			//create the record
			$this->values($parameters, $vaid_fields)->save();
			
			//return orm object
			return $this;
		}
		catch (ORM_Validation_Exception $e)
		{
			//throw excetion error
			$this->_throw_error($e);
		}
	}    	
}
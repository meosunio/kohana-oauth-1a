<?php defined('SYSPATH') or die('No direct script access.');

/**
 * The PLAINTEXT method does not provide any security protection and SHOULD only be used 
 * over a secure channel such as HTTPS. It does not use the Signature Base String.
 *   - Chapter 9.4 ("PLAINTEXT")
 */
class Oauth_Signature_PLAINTEXT extends Kohana_Oauth_Signature_PLAINTEXT
{
	
}

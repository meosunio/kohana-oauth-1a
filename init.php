<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * API Server Process
 */
Route::set('ApiAccessToken', 'server/access/token')
	->defaults(array(
		'controller' => 'Server',
		'action'     => 'access_token'
	));
	
Route::set('ApiRequestToken', 'server/request/token')
	->defaults(array(
		'controller' => 'Server',
		'action'     => 'request_token'
	));
	
Route::set('ApiDefault', 'server(/<action>)')
	->defaults(array(
		'controller' => 'Server',
		'action'     => 'index'
	));
	


/**
 * API Server Process
 */
Route::set('ClientAPICAlllToken', 'client/call/api')
	->defaults(array(
		'controller' => 'Client',
		'action'     => 'call_api'
	));
	
Route::set('ClientAccesToken', 'client/access/token')
	->defaults(array(
		'controller' => 'Client',
		'action'     => 'access_token'
	));
	
Route::set('ClientDefault', 'client(/<action>)')
	->defaults(array(
		'controller' => 'Client',
		'action'     => 'index'
	));
	
// Cache the routes in Production
Route::cache(Kohana::PRODUCTION === Kohana::$environment || Kohana::STAGING === Kohana::$environment);
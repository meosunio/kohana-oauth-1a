<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 *
 * @group oauth
 */
if (!class_exists('Mock_OAuthBaseStringRequest'))
{
	class Mock_OAuthBaseStringRequest
	{
		private $provided_base_string;
		public $base_string; // legacy
		public function __construct($bs) { $this->provided_base_string = $bs; }
		public function get_signature_base_string() { return $this->provided_base_string; }
	}

}

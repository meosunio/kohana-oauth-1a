<?php defined('SYSPATH') OR die('No direct access allowed.');

require MODPATH . 'oauth-1a/tests/common.php';

/**
 *
 * @group oauth.1a
 */
class OAuthConsumerTest extends Unittest_TestCase
{
	public function testConvertToString()
	{
		$consumer = new Oauth_Consumer('key', 'secret');
		$this->assertEquals('Oauth_Consumer[key=key,secret=secret]', (string) $consumer);
	}
}
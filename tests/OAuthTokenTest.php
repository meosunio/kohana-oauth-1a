<?php defined('SYSPATH') OR die('No direct access allowed.');

require MODPATH . 'oauth-1a/tests/common.php';

/**
 *
 * @group oauth.1a
 */
class OAuthTokenTest extends Unittest_TestCase {
	public function testSerialize() {
		$token = new Oauth_Token('token', 'secret');
		$this->assertEquals('{"oauth_token":"token","oauth_token_secret":"secret"}', $token->to_string());
		
	}
	public function testConvertToString() {
		$token = new Oauth_Token('token', 'secret');
		$this->assertEquals('{"oauth_token":"token","oauth_token_secret":"secret"}', (string) $token);

	}
}